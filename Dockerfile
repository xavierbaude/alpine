FROM alpine:3.7
LABEL MAINTAINER xavier.baude

USER 65534
CMD ["tail", "-f", "/dev/stdout"]
