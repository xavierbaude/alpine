# alpine

Non root alpine docker image.
Based from the official image, this image use the nobody user instead of root.

Image is located here : https://hub.docker.com/r/xavierbaude/alpine/
